import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import "bootstrap/dist/css/bootstrap.min.css"
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
import moment from 'moment';
import PrimeVue from 'primevue/config';
import VueLazyLoad from 'vue3-lazyload'

const app = createApp(App);
app.config.globalProperties.$axios = axios;
app.config.globalProperties.$moment = moment;
app.config.productionTip = false;
app.config.globalProperties.$filters = {
    strippedContent: function(string) {
        return string.replace(/<\/?[^>]+>/ig, " "); 
    }
}
app.use(VueLazyLoad, {
    loading: '',
    error: '',
    lifecycle: {
      loading: (el) => {
        console.log('loading', el)
      },
      error: (el) => {
        console.log('error', el)
      },
      loaded: (el) => {
        console.log('loaded', el)
      }
    }
  })
app.use(PrimeVue);
app.use(store);
app.use(router);
router.isReady().then(() => app.mount("#app"));
import "bootstrap/dist/js/bootstrap.js"
