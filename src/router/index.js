import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Workview from '../views/Work.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      title: 'Ideas',
      subtitle: 'Where all great thing'
    }
  },
  {
    path: '/work',
    name: 'work',
    component: Workview,
    meta: {
      title: 'Work',
      subtitle: 'Work Where all great thing'
    }
  },
  {
    path: '/about',
    name: 'about',
    component: Workview,
    meta: {
      title: 'About',
      subtitle: 'Abaout Where all great thing'
    }
  },
  {
    path: '/services',
    name: 'services',
    component: Workview,
    meta: {
      title: 'Services',
      subtitle: 'Services Where all great thing'
    }
  },
  {
    path: '/career',
    name: 'career',
    component: Workview,
    meta: {
      title: 'Carreer',
      subtitle: 'Carreer Where all great thing'
    }
  },
  {
    path: '/contact',
    name: 'contact',
    component: Workview,
    meta: {
      title: 'Contact',
      subtitle: 'Contact Where all great thing'
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
